<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>HỆ THỐNG MÁY BÁN HÀNG TỰ ĐỘNG - CẬP NHẬT THIẾT BỊ</title>
    <link rel="stylesheet" href="../css/capnhatthietbi_style.css">
</head>

<body>
    <?php
    header("Cache-Control: no cache");
    session_cache_limiter("private_no_expire");
    require_once '../src/database/db.php';
    require_once '../src/define.php';

    $loginOK = false;
    $user = "";

    // Check the permission
    session_start();
    if (isset($_SESSION['loginOK'])) {
        $loginOK = $_SESSION['loginOK'];
        $user = $_SESSION['username'];
    }
    if (!$loginOK) header('location: ../index.hpp');

    /* Check the code of products
    *   if the code is empty => new product
    *   else => update product with the code
    */

    $ma_may = "";
    $ma_sp = "";
    $ten_sp = "";
    $gram_factor = "";
    $gram_offset = "";
    $ml_factor = "";
    $ml_offset = "";

    echo '<pre>';
    print_r($_POST);
    echo '</pre>';

    if (isset($_POST["ma_may"])) $ma_may = $_POST["ma_may"];
    if (isset($_POST["s_ma_sp"])) $ma_sp = $_POST["s_ma_sp"];
    if (isset($_POST["s_ten_sp"])) $ten_sp = $_POST["s_ten_sp"];
    if (isset($_POST["gram_factor"])) $gram_factor = $_POST["gram_factor"];
    if (isset($_POST["gram_offset"])) $gram_offset = $_POST["gram_offset"];
    if (isset($_POST["ml_factor"])) $ml_factor = $_POST["ml_factor"];
    if (isset($_POST["ml_offset"])) $ml_offset = $_POST["ml_offset"];

    echo $ma_may  . '<br>';
    echo $ma_sp  . '<br>';
    echo $ten_sp  . '<br>';
    echo $gram_factor  . '<br>';
    echo $gram_offset  . '<br>';
    echo $ml_factor  . '<br>';
    echo $ml_offset  . '<br>';


    // function of get machine and product infomation

    $machineList = array();

    $sql = "SELECT * FROM machine";

    try {
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $machineList = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
    } catch (PDOException $e) {
        echo $e->getMessage();
    };

    echo '<pre>';
    print_r($machineList);
    echo '</pre>';

    $productList = array();

    $sql = "SELECT * FROM sanpham";

    try {
        // Get DB Object
        $db = new db();

        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        $productList = $stmt->fetchAll(PDO::FETCH_OBJ);

        // echo '<pre>';
        // print_r($productList);
        // echo '</pre>';

        // while($row = $stmt->fetch()) {
        //     echo '<pre>';
        //     print_r($row);
        //     echo '</pre>';
        // }
        $db = null;
    } catch (PDOException $e) {
        echo $e->getMessage();
    };



    // for ($i=0;$i<sizeof($machineList);$i++){
    //     $str = ' <option value="'. $i .'"'. $machineList[$i]->m_ma_may .'</option>';
    //     echo $str;
    // }




    // echo 'name: ' . $s_name . '<br>'

    ?>
    <div class="background">
        <div class="wrapper">
            <div class="menu">
                <ul>
                    <li><a href="s2main.php">MÀN HÌNH CHÍNH</a></li>
                    <li><a href="s300quanlysanpham.php">QUẢN LÝ SẢN PHẨM</a></li>
                    <li><a class="active" href="s400quanlythietbi.php">QUẢN LÝ THIẾT BỊ</a></li>
                    <li><a href="s500thongke.php">THỐNG KÊ</a></li>
                </ul>
            </div>
            <div class="container">
                <div class="page_title">
                    <h1>CẬP NHẬT THÔNG TIN THIẾT BỊ </h1>
                </div>
                <div class="content">
                    <form method="post" action="s430capnhatthietbi_action.php">
                        <div class="row">
                            <div class="row_title"> Mã máy: </div>
                            <div class="row_content">
                                <!-- <select name="ma_may"> -->
                                <?php
                                // for ($i = 0; $i < sizeof($machineList); $i++) {
                                //     $str = "";
                                // if ($machineList[$i]->m_ma_may === $ma_may)
                                //     $str = ' <option value="' . $machineList[$i]->m_ma_may . '" selected="true">' . $machineList[$i]->m_ma_may . '</option>';
                                // else
                                //     $str = ' <option value="' . $machineList[$i]->m_ma_may . '">' . $machineList[$i]->m_ma_may . '</option>';
                                $str = '<input type="text" readonly="readonly" name="ma_may" value="' . $ma_may . '" />';
                                echo $str;
                                // }
                                ?>
                                <!-- <option value="0">TLIE_MACHINE_001</option>;
                                    <option value="1">TLIE_MACHINE_002</option>;
                                    <option value="2">TLIE_MACHINE_003</option>; -->
                                <!-- </select> -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="row_title"> Mã sản phẩm: </div>
                            <div class="combobox">
                                <!-- <select name="ma_sanpham"> -->
                                <?php
                                // for ($i = 0; $i < sizeof($productList); $i++) {
                                //     $str = "";
                                //     if ($productList[$i]->s_ma_sp === $ma_sp)
                                //         $str = ' <option value="' . $productList[$i]->s_ma_sp . '" selected="true">' . $productList[$i]->s_ma_sp . '</option>';
                                //     else
                                //         $str = ' <option value="' . $productList[$i]->s_ma_sp . '">' . $productList[$i]->s_ma_sp . '</option>';
                                // }
                                $str = '<input type="text" readonly="readonly" name="ma_sanpham" value="' . $ma_sp . '" />';
                                echo $str;
                                ?>
                                <!-- <option value="0">SP_BOT_GIAT_OMO</option>
                                    <option value="1">SP_BOT_GIAT_OMO</option>
                                    <option value="2">SP_BOT_GIAT_OMO</option> -->
                                <!-- </select> -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="row_title"> Tên sản phẩm: </div>
                            <div class="textbox">
                                <?php
                                $str = '<input type="text" name="ten_sp" value="' . $ten_sp . '" />';
                                echo $str;
                                ?>
                            </div>
                        </div>
                        <?php

                        $str =
                            '<div class="row">
                                    <div class="row_title"> Hệ số round_to_gram_factor: </div>
                                    <div class="textbox">' .
                            '<input type="text" name="gram_factor" value="' . $gram_factor . '" />' .
                            '</div>
                                </div>' .
                            '<div class="row">
                                    <div class="row_title"> Hệ số round_to_gram_offset: </div>
                                    <div class="textbox">                                
                                        <input type="text" name="gram_offset" value="' . $gram_offset . '" />' .
                            '</div>
                                </div>' .
                            '<div class="row">
                                    <div class="row_title"> Hệ số round_to_ml_offset: </div>
                                    <div class="textbox">                                
                                        <input type="text" name="ml_factor" value="' . $ml_factor . '" />' .
                            '</div>
                                </div>' .
                            '<div class="row">
                                    <div class="row_title"> Hệ số round_to_ml_offset: </div>
                                    <div class="textbox">                                
                                        <input type="text" name="ml_offset" value="' . $ml_offset . '" />' .
                            '</div>
                                </div>';
                        echo $str;


                        ?>
                        <!-- <div class="row">
                            <div class="row_title"> Hệ số round_to_gram_factor: </div>
                            <div class="textbox">
                                //<?php
                                    // $str = '<input type="text" name="round_to_gram_factor" value="' . $gram_factor . '" />';
                                    // echo $str;
                                    
                                    ?>

                            // </div>
                        // </div>
                        // <div class="row">
                            // <div class="row_title"> Hệ số round_to_gram_offset: </div>
                            // <div class="textbox">
                            //<?php
                                //$str = '<input type="text" name="round_to_gram_offset" value="' . $gram_factor . '" />';
                                // echo $str;
                                
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row_title"> Hệ số round_to_ml_factor: </div>
                            <div class="textbox">
                                <input type="text" name="round_to_ml_factor" value="1.2" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="row_title"> Hệ số round_to_ml_offset: </div>
                            <div class="textbox">
                                <input type="text" name="round_to_ml_offset" value="1.2" />
                            </div>
                        </div> -->
                        <div> <button type="submit" name="submit">CẬP NHẬT </button>
                    </form>
                </div>
            </div>
        </div>

    </div>
    </div>
</body>

</html>