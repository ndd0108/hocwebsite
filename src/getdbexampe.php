<?php
require 'database/db.php';

$sql = "SELECT * FROM customers";
try {
    // Get DB Object
    $db = new db();
    // Connect
    $db = $db->connect();

    $stmt = $db->query($sql);
    $customer = $stmt->fetchAll(PDO::FETCH_OBJ);
    $db = null;
    // echo '<pre>';
    // print_r($customers);
    // echo '</pre>';
    // echo '<br />';
    echo json_encode($customer);
} catch (PDOException $e) {
    echo $e->getMessage();
};