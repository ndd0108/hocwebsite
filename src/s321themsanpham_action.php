<?php
require_once '../src/database/db.php';
require_once '../src/define.php';
$s_ma_sp = "";
$s_name = "";
$s_don_gia = "";
$s_ml_to_gram_factor = "";
$s_logo1 = "";
$s_logo2 = "";

echo '<pre>';
print_r($_POST);
echo '</pre>';

echo '<pre>';
print_r($_FILES);
echo '</pre>';

/**
 * GET VALUE
 */

if (isset($_POST["s_ma_sp"])) {
    $s_ma_sp = $_POST["s_ma_sp"];
}

if (isset($_POST["s_name"])) {
    $s_name = $_POST["s_name"];
}

if (isset($_POST["s_don_gia"])) {
    $s_don_gia = $_POST["s_don_gia"];
}

if (isset($_POST["s_ml_to_gram_factor"])) {
    $s_ml_to_gram_factor = $_POST["s_ml_to_gram_factor"];
}


/** 
 * UPLOAD IMAGES 1
 */
$target_dir = DIR_IMAGE_URL;

$target_file1 = $target_dir . basename($_FILES["img1"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file1, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if (isset($_POST["submit"])) {
    $check = getimagesize($_FILES["img1"]["tmp_name"]);
    if ($check !== false) {
        echo "File1 is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File1 is not an image.";
        $uploadOk = 0;
    }
}

echo 'target file 1: ' . $target_file1;
// Check if file already exists
if (file_exists($target_file1)) {
    echo "Sorry, file1 already exists.";
    $uploadOk = 0;
    $s_logo1 = basename($_FILES["img1"]["name"]);
}
// Check file size
if ($_FILES["img1"]["size"] > 500000) {
    echo "Sorry, your file 1 is too large.";
    $uploadOk = 0;
    $s_logo1 = "";
}
// Allow certain file formats
if (
    $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif"
) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
    $s_logo1 = "";
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["img1"]["tmp_name"], $target_file1)) {
        echo "The file " . basename($_FILES["img1"]["name"]) . " has been uploaded.";
        $s_logo1 = basename($_FILES["img1"]["name"]);
    } else {
        echo "Sorry, there was an error uploading your file.";
        $s_logo1 = "";
    }
}

/** 
 * UPLOAD IMAGES 2
 */
$target_file2 = $target_dir . basename($_FILES["img2"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file1, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if (isset($_POST["submit"])) {
    $check = getimagesize($_FILES["img2"]["tmp_name"]);
    if ($check !== false) {
        echo "File2 is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File2 is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file2)) {
    echo "Sorry, file2 already exists.";
    $s_logo2 = basename($_FILES["img2"]["name"]);
    $uploadOk = 0;
}
// Check file size
if ($_FILES["img2"]["size"] > 500000) {
    echo "Sorry, your file 2 is too large.";
    $uploadOk = 0;
    $s_logo2 = "";
}
// Allow certain file formats
if (
    $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif"
) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
    $s_logo2 = "";
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["img2"]["tmp_name"], $target_file2)) {
        echo "The file " . basename($_FILES["img2"]["name"]) . " has been uploaded.";
        $s_logo2 = basename($_FILES["img2"]["name"]);
    } else {
        echo "Sorry, there was an error uploading your file.";
        $s_logo2 = "";
    }
}

/**
 *  CREATE NEW PRODUCT IN DATABASE
 *  
 */

echo '<br> INSERT NEW ROW </br>';

$sql = "INSERT INTO sanpham (s_ma_sp,s_name,s_don_gia,s_ml_to_gram_factor,s_logo1,s_logo2) 
                            VALUE(:s_ma_sp,:s_name,:s_don_gia,:s_ml_to_gram_factor,:s_logo1,:s_logo2)";

echo 'sql query: ' . $sql;
try {
    // Get DB Object
    $db = new db();
    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':s_ma_sp',                  $s_ma_sp);
    $stmt->bindParam(':s_name',                   $s_name);
    $stmt->bindParam(':s_don_gia',                $s_don_gia);
    $stmt->bindParam(':s_ml_to_gram_factor',      $s_ml_to_gram_factor);
    $stmt->bindParam(':s_logo1',                  $s_logo1);
    $stmt->bindParam(':s_logo2',                  $s_logo2);

    $stmt->execute();

    echo '<br> {"notice": {"text": "New product Addded: "' . $s_ma_sp .'"} }';
} catch (PDOException $e) {
    echo $e->getMessage();
};

header('location: s300quanlysanpham.php');