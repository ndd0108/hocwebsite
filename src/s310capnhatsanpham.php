<?php
require_once '../src/database/db.php';
require_once '../src/define.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>HỆ THỐNG MÁY BÁN HÀNG TỰ ĐỘNG - CẬP NHẬT SẢN PHẨM</title>
    <link rel="stylesheet" href="../css/capnhatsanpham_style.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#link1').change(function(e) {
                var fileName = e.target.files[0].name; //get file name

                filePath = URL.createObjectURL(event.target.files[0]); // get template path

                $('#img1').attr("src", filePath);
            });

            $('#link2').change(function(e) {
                var fileName = e.target.files[0].name; //get file name

                filePath = URL.createObjectURL(event.target.files[0]); // get template path

                $('#img2').attr("src", filePath);
            });
        });
    </script>

</head>

<body>
    <?php

    // function readProductInfo($code = "")
    // {
    //     $result = array();

    //     $sql = "SELECT * FROM sanpham WHERE s_ma_sp='$code'";

    //     try {
    //         // Get DB Object
    //         $db = new db();
    //         // Connect
    //         $db = $db->connect();

    //         $stmt = $db->query($sql);
    //         $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    //         $db = null;
    //     } catch (PDOException $e) {
    //         echo $e->getMessage();
    //     };

    //     return $result;
    // }

    $loginOK = false;
    $user = "";

    // Check the permission
    session_start();
    if (isset($_SESSION['loginOK'])) {
        $loginOK = $_SESSION['loginOK'];
        $user = $_SESSION['username'];
    }
    if (!$loginOK) header('location: ../index.hpp');

    /* Check the code of products
    *   if the code is empty => new product
    *   else => update product with the code
    */
    $id = 0;
    $s_ma_sp = "";
    $s_name = "";
    $s_don_gia = "";
    $s_ml_to_gram_factor = "";
    $s_logo1 = "";
    $s_logo2 = "";
    $title = "THÊM SẢN PHẨM MỚI";
    if (isset($_GET["code"]))
        $s_ma_sp = $_GET["code"];

    $productInfo = readProductInfo($s_ma_sp);


    if (!empty($productInfo)) {
        $id = $productInfo[0]->id;
        $s_name = ($productInfo[0]->s_name);
        $s_don_gia = $productInfo[0]->s_don_gia;
        $s_ml_to_gram_factor = $productInfo[0]->s_ml_to_gram_factor;
        $s_logo1 = $productInfo[0]->s_logo1;
        $s_logo2 = $productInfo[0]->s_logo2;
        $title = "THAY ĐỔI THÔNG TIN SẢN PHẨM";
    }

    // echo 'name: ' . $s_name . '<br>'

    ?>
    <div class="background">
        <div class="wrapper">
            <div class="menu">
                <ul>
                    <li><a href="s2main.php">MÀN HÌNH CHÍNH</a></li>
                    <li><a class="active" href="s300quanlysanpham.php">QUẢN LÝ SẢN PHẨM</a></li>
                    <li><a href="s400quanlythietbi.php">QUẢN LÝ THIẾT BỊ</a></li>
                    <li><a href="s500thongke.php">THỐNG KÊ</a></li>
                </ul>
            </div>
            <div class="container">

                <form class="form" action="s311capnhatsanpham_action.php" method="post" enctype="multipart/form-data">
                    <?php
                    $str = '<div class="title"> <h1>' . $title . ' </h1></div>';
                    echo $str;
                    ?>
                    <div class="row">
                        <div class="title"> id: </div>
                        <?php
                        $str = '<div class="textbox"><input readonly="readonly" type="text" name="id" value="' . $id . '" /></div>';
                        echo $str;
                        ?>
                        <!-- <div class="textbox"><input type="text" name="s_ma_sp" value="SP_BOT_GIAT_OMO_001" /></div> -->
                    </div>
                    <div class="row">
                        <div class="title"> Mã Sản Phẩm: </div>
                        <?php
                        $str = '<div class="textbox"><input type="text" name="s_ma_sp" value="' . $s_ma_sp . '" /></div>';
                        echo $str;
                        ?>
                        <!-- <div class="textbox"><input type="text" name="s_ma_sp" value="SP_BOT_GIAT_OMO_001" /></div> -->
                    </div>

                    <div class="row">
                        <div class="title"> Tên Sản Phẩm: </div>
                        <?php
                        $str = '<div class="textbox"> <input type="text" name="s_name" value="' . $s_name . '" /></div>';
                        echo $str;
                        ?>
                        <!-- <div class="textbox"> <input type="text" value="Bột Giặt OMO" /></div> -->
                        <div class="clear"></div>
                    </div>

                    <div class="row">
                        <div class="title"> Đơn giá (đồng/kg): </div>
                        <?php
                        $str = '<div class="textbox"><input type="text" name="s_don_gia" value="' . $s_don_gia . '" /></div>';
                        echo $str;
                        ?>
                    </div>

                    <div class="row">
                        <div class="title"> Tỷ lệ (gram/ml): </div>
                        <?php
                        $str = '<div class="textbox"> <input type="text" name="s_ml_to_gram_factor" value="' . $s_ml_to_gram_factor . '" /> </div>';
                        echo $str;
                        ?>
                    </div>

                    <div class="img_group">
                        <div class="image_box1">
                            <p> LOGO1 </p>
                            <?php
                            $path = DIR_IMAGE_URL . $s_logo1;
                            $str = '<div class="img_frame"> <img src="' . $path . '" id="img1"/></div>';
                            echo $str;
                            ?>
                            <div class="img">
                                <input type="file" name="img1" id="link1">
                            </div>
                        </div>

                        <div class="image_box2">
                            <p> LOGO2 </p>
                            <?php
                            $path = DIR_IMAGE_URL . $s_logo2;
                            $str = '<div class="img_frame"> <img src="' . $path . '" id="img2"/></div>';
                            echo $str;
                            ?>
                            <div class="img">
                                <input type="file" name="img2" id="link2">
                            </div>
                        </div>
                    </div>

                    <div> <button type="submit" id="submit">XÁC NHẬN THAY ĐỔI</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>