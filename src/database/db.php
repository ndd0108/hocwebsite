<?php
class db
{
    // Properties
    private $dbhost  = 'localhost';
    private $dbuser  = 'root';
    private $dbpass = '123456';
    private $dbname = 'thanlongie_banhangtudong';

    // Connect            
    public function connect()
    {
        $mysql_connect_str = "mysql:host=$this->dbhost;dbname=$this->dbname;charset=utf8";
        $dbConnection = new PDO($mysql_connect_str, $this->dbuser, $this->dbpass);
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbConnection;
    }
}


function readAccount($user, $pass)
    {
        $result = false;

        $sql = "SELECT * FROM account_manager 
                           WHERE username='$user' AND password='$pass'";

        try {
            // Get DB Object
            $db = new db();
            // Connect
            $db = $db->connect();

            $stmt = $db->query($sql);
            $account = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            if (!empty($account))
                $result = true;
        } catch (PDOException $e) {
            echo $e->getMessage();
        };

        return $result;
    }

class Machine_Product
{
    // private $ma_may = "";
    // private $ma_sp = "";
    // private $ten_sp = "";
    // private $logo1 = "";
    // private $logo2 = "";
    // private $dongia = 0;
    // private $ml_to_gram_factor = 0;
    // private $round_to_gram_factor = 0;
    // private $round_to_gram_offset = 0;
    // private $round_to_ml_factor = 0;
    // private $round_to_ml_offset = 0;
    // private $pos=0;

    function setMachineID($ma_may)
    {
        $this->ma_may = $ma_may;
    }

    function setProduct($ma_sp, $ten_sp, $dongia, $logo1, $logo2, $ml_to_gram_factor, $pos, $type)
    {
        $this->ma_sp = $ma_sp;
        $this->ten_sp = $ten_sp;
        $this->dongia = $dongia;
        $this->logo1 = $logo1;
        $this->logo2 = $logo2;
        $this->ml_to_gram_factor = $ml_to_gram_factor;
        $this->pos = $pos;
        $this->type = $type;
    }
    function setStepperParameters($round_2_gram, $round_2_gram_offset, $round_2_ml, $round_2_ml_offset)
    {
        $this->round_to_gram_factor = $round_2_gram;
        $this->round_to_gram_offset = $round_2_gram_offset;
        $this->round_to_ml_factor = $round_2_ml;
        $this->round_to_ml_offset = $round_2_ml_offset;
    }
}

function readProductName($s_ma_sp)
{
    $result = "";

    $sql = "SELECT s_name FROM sanpham WHERE s_ma_sp='$s_ma_sp'";

    try {
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
    } catch (PDOException $e) {
        echo $e->getMessage();
    };

    return $result;
}

function readProductInfo($s_ma_sp)
{
    $result = "";

    $sql = "SELECT * FROM sanpham WHERE s_ma_sp='$s_ma_sp'";

    try {
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
    } catch (PDOException $e) {
        echo $e->getMessage();
    };

    return $result;
}

function readMachinesList()
{
    $result = array();

    $sql = "SELECT * FROM machine";

    try {
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
    return $result;
}

function readProduct_Machine($m_ma_may)
{
    $result = array();
    $sql = "SELECT * FROM machine_sanpham_relation WHERE m_ma_may='$m_ma_may'";

    // echo $sql;

    try {
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
    return $result;
}

function readAvailableProduct($m_ma_may)
{
    $result = array();
    $sql = "SELECT m_ma_sp FROM machine_sanpham_relation WHERE m_ma_may='$m_ma_may'";

    // echo $sql;

    try {
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
    return $result;
}



function readParamOfMachine($mamay)
{
    $result = array();
    $sql = "SELECT * FROM machine_sanpham_relation WHERE m_ma_may='$mamay' ORDER BY pos";
    $db = new db();
    // Connect
    $db = $db->connect();

    $stmt = $db->query($sql);
    while ($row = $stmt->fetch()) {
        $pr1 = new Machine_Product();
        $ma_may = $row["m_ma_may"];
        $ma_sp = $row["m_ma_sp"];
        $sp = readProductInfo($row["m_ma_sp"])[0];
        $ten_sp = $sp->s_name;
        $ml_to_gram_factor = (double)$sp->s_ml_to_gram_factor;
        $don_gia = (double)$sp->s_don_gia;
        $logo1 = $sp->s_logo1;
        $logo2 = $sp->s_logo2;
        $type = (int)$sp->type;

        $round_2_gram = (double)$row["m_round_to_gram_factor"];
        $round_2_gram_offset = (double)$row["m_round_to_gram_offset"];
        $round_2_ml = (double)$row["m_round_to_ml_factor"];
        $round_2_ml_offset = (double)$row["m_round_to_ml_offset"];
        $pos =  (int)$row["pos"];

        $pr1->setMachineID($ma_may);
        $pr1->setProduct($ma_sp, $ten_sp, $don_gia, $logo1, $logo2, $ml_to_gram_factor, $pos, $type);
        $pr1->setStepperParameters($round_2_gram, $round_2_gram_offset, $round_2_ml, $round_2_ml_offset);

        $result[] = $pr1;
    }
    $db = null;

    return $result;
}

