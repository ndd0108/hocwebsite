<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HỆ THỐNG MÁY BÁN HÀNG TỰ ĐỘNG - QUẢN LÝ SẢN PHẨM</title>
    <link rel="stylesheet" href="../css/quanlysanpham_style2.css">
</head>

<body>
    <?php
    // this code for prevent the error: confirm form resubmission... 
    header("Cache-Control: no cache");
    session_cache_limiter("private_no_expire");

    require_once 'define.php';
    require_once 'database/db.php';
    function readProductsList()
    {
        $result = array();

        $sql = "SELECT * FROM sanpham";

        try {
            // Get DB Object
            $db = new db();
            // Connect
            $db = $db->connect();

            $stmt = $db->query($sql);
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
        } catch (PDOException $e) {
            echo $e->getMessage();
        };

        return $result;
    }

    function createProductInfo($productInfo = array())
    {
        // Đầu vào thuộc kiểu stdClass Object, lấy dữ liệu như 1 đối tượng
        $str = "";
        $id = $productInfo->id;
        $ma_sp = $productInfo->s_ma_sp;
        $name = $productInfo->s_name;
        $don_gia = $productInfo->s_don_gia;
        $factor = $productInfo->s_ml_to_gram_factor;
        $logo1 = DIR_IMAGE_URL . $productInfo->s_logo1;
        $logo2 = DIR_IMAGE_URL . $productInfo->s_logo2;

        $str = $str .
            '<div class="content">
        <div class="left">
                <div class="image_content1">
                    <img src="' . $logo1 . '">
                </div>
                <div class="image_content2">
                    <img src="' . $logo2 . '">
                </div>

        </div>
        <div class="right">  
            <div class="row">
                <div class="title"> Tên Sản Phẩm: </div>
                <div class="textbox"><input type="text" name="s_name" readonly="readonly" value="' . $name . '" /></div>
            </div>
            <div class="row">
                <div class="title"> Đơn giá đ/kg: </div>
                <div class="textbox"><input type="text" name="s_dongia" readonly="readonly" value="' . $don_gia . '" /></div>
            </div>
            <div class="row">
                <div class="title"> Tỷ lệ gram/ml: </div>
                <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="' . $factor . '" /></div>
            </div>
            <div>
                <form class="form" method="get" action="s310capnhatsanpham.php">
                    <div class="row">
                        <div class="title"> Mã sản phẩm: </div>
                        <div class="textbox"><input type="text" name="code" readonly="readonly" value="' . $ma_sp . '" /></div>
                    </div>
                    <div> <button type="submit"> Thay đổi thông tin</button></div>
                </form>
            </div>
        </div>
    </div>
  ';


        return $str;
    }

    $loginOK = false;

    session_start();
    if (isset($_SESSION['loginOK']))
        $loginOK = $_SESSION['loginOK'];

    if (!$loginOK) header('location: ../index.php');

    $productList = readProductsList();
    echo '<pre>';
    print_r($productList);
    echo '</pre>';

    ?>
    <div class="background">
        <div class="wrapper">
            <div class="menu">
                <ul>
                    <li><a href="s2main.php">MÀN HÌNH CHÍNH</a></li>
                    <li><a class = "active" href="s300quanlysanpham.php">QUẢN LÝ SẢN PHẨM</a></li>
                    <li><a href="s400quanlythietbi.php">QUẢN LÝ THIẾT BỊ</a></li>
                    <li><a href="s500thongke.php">THỐNG KÊ</a></li>
                </ul>
            </div>
            <h1> DANH SÁCH SẢN PHẨM </h1>
            <div class="container">
                <?php
                for ($i = 0; $i < sizeof($productList); $i++) {
                    $str = createProductInfo($productList[$i]);
                    echo $str;
                }
                ?>
                <!-- <div class="content">
                    <div class="left">
                        <div class="image_content1">
                            <img src="../images/omo1.jpg">
                        </div>
                        <div class="image_content2">
                            <img src="../images/omo2.png">
                        </div>

                    </div>
                    <div class="right">
                        <div class="row">
                            <div class="title"> Tên Sản Phẩm: </div>
                            <div class="textbox"><input type="text" name="s_name" readonly="readonly" value="Bột giặt OMO" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Đơn giá: </div>
                            <div class="textbox"><input type="text" name="s_dongia" readonly="readonly" value="30000" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Tỷ lệ gram/ml: </div>
                            <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="1.5" /></div>
                        </div>
                        <div>
                            <form class="form">
                                <div class="row">
                                    <div class="title"> Mã sản phẩm: </div>
                                    <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="SP_BOT_GIAT_OMO_001" /></div>
                                </div>
                                <div> <button type="submit" name="submit"> Thay đổi thông tin</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="left">
                        <div class="image_content1">
                            <img src="../images/omo1.jpg">
                        </div>
                        <div class="image_content2">
                            <img src="../images/omo2.png">
                        </div>

                    </div>
                    <div class="right">
                        <div class="row">
                            <div class="title"> Tên Sản Phẩm: </div>
                            <div class="textbox"><input type="text" name="s_name" readonly="readonly" value="Bột giặt OMO" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Đơn giá: </div>
                            <div class="textbox"><input type="text" name="s_dongia" readonly="readonly" value="30000" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Tỷ lệ gram/ml: </div>
                            <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="1.5" /></div>
                        </div>
                        <div>
                            <form class="form">
                                <div class="row">
                                    <div class="title"> Mã sản phẩm: </div>
                                    <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="SP_BOT_GIAT_OMO_001" /></div>
                                </div>
                                <div> <button type="submit" name="submit"> Thay đổi thông tin</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="left">
                        <div class="image_content1">
                            <img src="../images/omo1.jpg">
                        </div>
                        <div class="image_content2">
                            <img src="../images/omo2.png">
                        </div>

                    </div>
                    <div class="right">
                        <div class="row">
                            <div class="title"> Tên Sản Phẩm: </div>
                            <div class="textbox"><input type="text" name="s_name" readonly="readonly" value="Bột giặt OMO" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Đơn giá: </div>
                            <div class="textbox"><input type="text" name="s_dongia" readonly="readonly" value="30000" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Tỷ lệ gram/ml: </div>
                            <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="1.5" /></div>
                        </div>
                        <div>
                            <form class="form">
                                <div class="row">
                                    <div class="title"> Mã sản phẩm: </div>
                                    <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="SP_BOT_GIAT_OMO_001" /></div>
                                </div>
                                <div> <button type="submit" name="submit"> Thay đổi thông tin</button>
                            </form>
                        </div>
                    </div>
                </div> -->


            </div>
            <div class="submit_new_product">
                <form action="s320themsanpham.php" method="post">
                    <button type="submit">THÊM SẢN PHẨM MỚI</button>
                </form>
            </div>

        </div>
    </div>
</body>

</html>