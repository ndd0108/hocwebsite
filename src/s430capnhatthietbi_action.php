<?php
require_once '../src/database/db.php';
require_once '../src/define.php';

echo '<pre>';
print_r($_POST);
echo '</pre>';

$ma_may = "";
$ma_sanpham = "";
$ten_sp = "";
$round_to_gram_factor = 0;
$round_to_gram_offset = 0;
$round_to_ml_factor = 0;
$round_to_ml_offset = 0;

if (isset($_POST["ma_may"]))
    $ma_may = $_POST["ma_may"];

if (isset($_POST["ma_sanpham"]))
    $ma_sanpham = $_POST["ma_sanpham"];

if (isset($_POST["ten_sp"]))
    $ten_sp = $_POST["ten_sp"];

if (isset($_POST["gram_factor"]))
    $round_to_gram_factor = $_POST["gram_factor"];
if (isset($_POST["gram_offset"]))
    $round_to_gram_offset = $_POST["gram_offset"];
if (isset($_POST["ml_factor"]))
    $round_to_ml_factor = $_POST["ml_factor"];
if (isset($_POST["ml_offset"]))
    $round_to_ml_offset = $_POST["ml_offset"];

  
    echo "<br> ma_may ". $ma_may;
    echo "<br> ma_sanpham ". $ma_sanpham;
    echo "<br> ten_sp ". $ten_sp;
    echo "<br> round_to_gram_factor " . $round_to_gram_factor;
    echo "<br> round_to_gram_offse " . $round_to_gram_offset;
    echo "<br> round_to_ml_factor " . $round_to_ml_factor;
    echo "<br> round_to_ml_offset " . $round_to_ml_offset . '<br>';




$sql = "UPDATE machine_sanpham_relation SET             	
                m_round_to_gram_factor     = :round_to_gram_factor,
                m_round_to_gram_offset     = :round_to_gram_offset,
             	m_round_to_ml_factor       = :round_to_ml_factor,
                m_round_to_ml_offset       = :round_to_ml_offset
        WHERE m_ma_may = :ma_may AND m_ma_sp = :ma_sanpham";

echo 'sql query: <br>' . $sql . '<br>';
try {
    // Get DB Object
    $db = new db();
    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':ma_sanpham',               $ma_sanpham);
    $stmt->bindParam(':ma_may',                   $ma_may);
    $stmt->bindParam(':round_to_gram_factor',     $round_to_gram_factor);
    $stmt->bindParam(':round_to_gram_offset',     $round_to_gram_offset);
    $stmt->bindParam(':round_to_ml_factor',       $round_to_ml_factor);
    $stmt->bindParam(':round_to_ml_offset',       $round_to_ml_offset);

    $stmt->execute();

    echo '<br> {"notice": {"text": "Product updated: "';
} catch (PDOException $e) {
    echo $e->getMessage();
};

header('location: s400quanlythietbi.php');
