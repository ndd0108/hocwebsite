<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>HỆ THỐNG MÁY BÁN HÀNG TỰ ĐỘNG - CẬP NHẬT SẢN PHẨM</title>
    <link rel="stylesheet" href="../css/capnhatsanpham_style2.css">
</head>

<body>
    <?php
    header("Cache-Control: no cache");
    session_cache_limiter("private_no_expire");
    require_once '../src/database/db.php';
    require_once '../src/define.php';
    function readProductInfo($code = "")
    {
        $result = array();

        $sql = "SELECT * FROM sanpham WHERE s_ma_sp='$code'";

        try {
            // Get DB Object
            $db = new db();
            // Connect
            $db = $db->connect();

            $stmt = $db->query($sql);
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
        } catch (PDOException $e) {
            echo $e->getMessage();
        };

        return $result;
    }

    $loginOK = false;

    // Check the permission
    session_start();
    if (isset($_SESSION['loginOK']))
        $loginOK = $_SESSION['loginOK'];
    if (!$loginOK) header('location: ../index.hpp');

    /* Check the code of products
    *   if the code is empty => new product
    *   else => update product with the code
    */
    $s_ma_sp = "";
    $s_name = "";
    $s_don_gia = "";
    $s_ml_to_gram_factor = "";
    $s_logo1 = "";
    $s_logo2 = "";
    $title = "THÊM SẢN PHẨM MỚI";
    if (isset($_GET["code"]))
        $s_ma_sp = $_GET["code"];

    $productInfo = readProductInfo($s_ma_sp);


    if (!empty($productInfo)) {
        $s_name = ($productInfo[0]->s_name);
        $s_don_gia = $productInfo[0]->s_don_gia;
        $s_ml_to_gram_factor = $productInfo[0]->s_ml_to_gram_factor;
        $s_logo1 = $productInfo[0]->s_logo1;
        $s_logo2 = $productInfo[0]->s_logo2;
        $title = "THAY ĐỔI THÔNG TIN SẢN PHẨM";
    }

    // echo 'name: ' . $s_name . '<br>'

    ?>
    <div class="background">
        <div class="wrapper">
            <div class="container">
                <form class="form" method="post" action="s321themsanpham_action.php" enctype="multipart/form-data">
                    <?php
                    $str = '<div class="title"> <h1>'.$title.' </h1></div>';
                    echo $str;
                    ?>
                    <div class="row">
                        <div class="title"> Mã Sản Phẩm: </div>
                        <?php
                        $str = '<div class="textbox"><input type="text" name="s_ma_sp" value="' . $s_ma_sp . '" /></div>';
                        echo $str;
                        ?>
                        <!-- <div class="textbox"><input type="text" name="s_ma_sp" value="SP_BOT_GIAT_OMO_001" /></div> -->
                    </div>

                    <div class="row">
                        <div class="title"> Tên Sản Phẩm: </div>
                        <?php
                        $str = '<div class="textbox"> <input type="text" name="s_name" value="' . $s_name . '" /></div>';
                        echo $str;
                        ?>
                        <!-- <div class="textbox"> <input type="text" value="Bột Giặt OMO" /></div> -->
                        <div class="clear"></div>
                    </div>

                    <div class="row">
                        <div class="title"> Đơn giá (đồng/kg): </div>
                        <?php
                        $str = '<div class="textbox"><input type="text" name="s_don_gia" value="' . $s_don_gia . '" /></div>';
                        echo $str;
                        ?>
                        <!-- <div class="textbox"><input type="text" name="s_don_gia" value="30000" /></div> -->
                    </div>

                    <div class="row">
                        <div class="title"> Tỷ lệ (gram/ml): </div>
                        <?php
                        $str = '<div class="textbox"> <input type="text" name="s_ml_to_gram_factor" value="' . $s_ml_to_gram_factor . '" /> </div>';
                        echo $str;
                        ?>
                        <!-- <div class="textbox"> <input type="text" name="s_name" value="1.0" /> </div> -->
                    </div>

                    <div class="img_group">
                        <div class="image_box1">
                            <p> LOGO1 </p>
                            <?php
                            $path = DIR_IMAGE_URL . $s_logo1;
                            $str = '<div> <img src="' . $path . '" /></div>';
                            echo $str;
                            ?>
                            <!-- <div> <img src="../images/omo1.jpg" /></div> -->

                            <div class="img">
                                <input type='file' name="img1" id="fileToUpload1">
                            </div>
                        </div>

                        <div class="image_box2">
                            <p> LOGO2 </p>
                            <?php
                            $path = DIR_IMAGE_URL . $s_logo2;
                            $str = '<div> <img src="' . $path . '" /></div>';
                            echo $str;
                            ?>
                            <!-- <div> <img src="../images/omo2.png" /></div> -->

                            <div class="img">
                                <input type='file' name="img2" id="fileToUpload2">
                            </div>
                        </div>
                    </div>

                    <div> <button type="submit" id = "submit">SAVE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>