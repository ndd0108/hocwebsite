<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HỆ THỐNG MÁY BÁN HÀNG TỰ ĐỘNG - ĐĂNG NHẬP</title>
    <link rel="stylesheet" href="../css/login_style.css">
</head>

<body>
    <?php
    $login_stt = "true";
    if (isset($_GET['login_stt'])) {
        $login_stt = $_GET['login_stt'];
    }
    $status = $login_stt === 'false' ? false : true;

    $loginOK = false;

    session_start();
    if (isset($_SESSION['loginOK']))
        $loginOK = $_SESSION['loginOK'];

    if ($loginOK) {
        header('location: ../src/s2main.php');
    }

    ?>

    <div class="wrapper">
        <div class="container">
            <h1>Xin chào</h1>

            <form class="form" action="../src/s2main.php" method="post">
                <div><input type="user" placeholder="Username" name="username"></div>
                <div><input type="password" placeholder="Password" name="password"></div>
                <div><button type="submit" id="login-button">Đăng Nhập</button></div>

                <?php
                if (!$status) {
                    $str = '';
                    $str .= '<div class="thongbao">';
                    $str .= 'Quý khách đã nhập sai thông tin <span> Username</span> hoặc <span>Password</span>';
                    $str .= '</div>';
                    echo $str;
                }
                ?>

                <!-- <div class = "thongbao">
                Quý khách đã nhập sai thông tin <b>Username</b> hoặc <b>Password</b>
                </div> -->
            </form>
        </div>
    </div>




</body>

</html>