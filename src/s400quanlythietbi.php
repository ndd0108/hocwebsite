<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HỆ THỐNG MÁY BÁN HÀNG TỰ ĐỘNG - QUẢN LÝ THIẾT BỊ </title>
    <link rel="stylesheet" href="../css/quanlythietbi_style.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            // $("button").click(function() {
            //     var m_ma_may = $(this).attr("name");

            //     $.post("s41cauhinhthietbi.php", {
            //         code: m_ma_may,
            //     });
            //     alert("button click:" + m_ma_may);
            // });
        });
    </script>

</head>

<body>
    <?php
    // this code for prevent the error: confirm form resubmission... 
    header("Cache-Control: no cache");
    session_cache_limiter("private_no_expire");

    require_once 'define.php';
    require_once 'database/db.php';

    $machineList = array();


    function createMachineDisplay($machineList, $index)
    {
        $m_ma_may = "";
        $m_machine_status = "Không hoạt động";
        $productNameArray = array();
        $productStatusArray = array();

        if (!empty($machineList)) {
            $m_ma_may = $machineList[$index]->m_ma_may;
            $m_machine_status = $machineList[$index]->m_active_stt == 1 ? "Đang hoạt động" : "Không hoạt động";
            $product_machine_list = readProduct_Machine($machineList[$index]->m_ma_may);
            // echo '<pre>'; print_r($product_machine_list);echo '</pre>'; 
            for ($i = 0; $i < sizeof($product_machine_list); $i++) {
                $productName = readProductName($product_machine_list[$i]->m_ma_sp)[0]->s_name;
                $productNameArray[] = $productName;
                $productStatusArray[] = $product_machine_list[$i]->m_current_level;
            }
        }

        // echo $m_ma_may . '<br>';
        // echo $m_machine_status . '<br>';

        // echo '<pre>'; print_r($productNameArray);echo '</pre>'; 
        // echo '<pre>'; print_r($productStatusArray);echo '</pre>'; 

        $cmdProduct  = "";
        if (!empty($productNameArray))
            $cmdProduct .= '<span>' . $productNameArray[0] . '</span>';
        for ($i = 1; $i < sizeof($productNameArray); $i++) {
            $cmdProduct .= ' , <span>' . $productNameArray[$i] . '</span>';
        }

        $cmdStatus  = "";
        if (!empty($productStatusArray))
            $cmdStatus .= '<span>' . $productStatusArray[0] . '%</span>';
        for ($i = 1; $i < sizeof($productStatusArray); $i++) {
            $cmdStatus .= ' , <span>' . $productStatusArray[$i] . '%</span>';
        }



        $str = '<div class="content">
        <div class="row">
            <div class="row_title">
                Mã thiết bị:
            </div>
            <div class="textbox"><input type="text" name="ma_thiet_bi" readonly="readonly" value="' . $m_ma_may . '" /></div>
        </div>

        <div class="row">
            <div class="row_title">
                Trạng thái hoạt động: 
            </div>
            <div class="machine_status_content">
                ' . $m_machine_status . '
            </div>
        </div>

        <div class="row">
            <div class="row_title">
                Sản phẩm được bán: 
            </div>
            <div class="product_content">
               ' . $cmdProduct . '
            </div>
        </div>

        <div class="row">
            <div class="row_title">
                Trạng thái sản phẩm: 
            </div>
            <div>
                ' . $cmdStatus . '
            </div>
        </div>

        <div>
        <form method="post" action="s410cauhinhthietbi.php"> 
            <button type="submit" name="' . $m_ma_may  . '">Cập nhật thông tin</button>
        </form>
        </div>
    </div>';
        return $str;
    }



    $loginOK = false;

    session_start();
    if (isset($_SESSION['loginOK']))
        $loginOK = $_SESSION['loginOK'];

    if (!$loginOK) header('location: ../index.php');

    $machineList = readMachinesList();
    echo '<pre>';
    print_r($machineList);
    echo '</pre>';

    // for ($i = 0; $i < sizeof($machineList); $i++) {
    //     createMachineDisplay($machineList, $i);
    // }

    ?>
    <div class="background">
        <div class="wrapper">
            <div class="menu">
                <ul>
                    <li><a href="s2main.php">MÀN HÌNH CHÍNH</a></li>
                    <li><a href="s300quanlysanpham.php">QUẢN LÝ SẢN PHẨM</a></li>
                    <li><a class="active" href="s400quanlythietbi.php">QUẢN LÝ THIẾT BỊ</a></li>
                    <li><a href="s500thongke.php">THỐNG KÊ</a></li>
                </ul>
            </div>
            <h1> DANH SÁCH THIẾT BỊ </h1>
            <div class="container">
                <?php
                for ($index = 0; $index < sizeof($machineList); $index++) {
                    echo createMachineDisplay($machineList, $index);
                }
                ?>
                <!-- <div class="content">
                    <div class="row">
                        <div class="row_title">
                            Mã thiết bị:
                        </div>
                        <div class="machine_code">
                            TLIE_MACHINE_001
                        </div>
                    </div>

                    <div class="row">
                        <div class="row_title">
                            Trạng thái hoạt động:
                        </div>
                        <div class="machine_status_content">
                            Đang hoạt động
                        </div>
                    </div>

                    <div class="row">
                        <div class="row_title">
                            Sản phẩm được bán:
                        </div>
                        <div class="product_content">
                            <span> Bột Giặt OMO </span> , <span> Dầu gôi Clear </span> , <span> Dầu ăn Neptune </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="row_title">
                            Trạng thái sản phẩm:
                        </div>
                        <div>
                            <span class="product_status"> 30% </span> , <span class="product_status"> 50% </span> , <span class="product_status"> 20% </span>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="content">
                    <div class="left">
                        <div class="image_content1">
                            <img src="../images/omo1.jpg">
                        </div>
                        <div class="image_content2">
                            <img src="../images/omo2.png">
                        </div>

                    </div>
                    <div class="right">
                        <div class="row">
                            <div class="title"> Tên Sản Phẩm: </div>
                            <div class="textbox"><input type="text" name="s_name" readonly="readonly" value="Bột giặt OMO" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Đơn giá: </div>
                            <div class="textbox"><input type="text" name="s_dongia" readonly="readonly" value="30000" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Tỷ lệ gram/ml: </div>
                            <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="1.5" /></div>
                        </div>
                        <div>
                            <form class="form">
                                <div class="row">
                                    <div class="title"> Mã sản phẩm: </div>
                                    <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="SP_BOT_GIAT_OMO_001" /></div>
                                </div>
                                <div> <button type="submit" name="submit"> Thay đổi thông tin</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="left">
                        <div class="image_content1">
                            <img src="../images/omo1.jpg">
                        </div>
                        <div class="image_content2">
                            <img src="../images/omo2.png">
                        </div>

                    </div>
                    <div class="right">
                        <div class="row">
                            <div class="title"> Tên Sản Phẩm: </div>
                            <div class="textbox"><input type="text" name="s_name" readonly="readonly" value="Bột giặt OMO" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Đơn giá: </div>
                            <div class="textbox"><input type="text" name="s_dongia" readonly="readonly" value="30000" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Tỷ lệ gram/ml: </div>
                            <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="1.5" /></div>
                        </div>
                        <div>
                            <form class="form">
                                <div class="row">
                                    <div class="title"> Mã sản phẩm: </div>
                                    <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="SP_BOT_GIAT_OMO_001" /></div>
                                </div>
                                <div> <button type="submit" name="submit"> Thay đổi thông tin</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="left">
                        <div class="image_content1">
                            <img src="../images/omo1.jpg">
                        </div>
                        <div class="image_content2">
                            <img src="../images/omo2.png">
                        </div>

                    </div>
                    <div class="right">
                        <div class="row">
                            <div class="title"> Tên Sản Phẩm: </div>
                            <div class="textbox"><input type="text" name="s_name" readonly="readonly" value="Bột giặt OMO" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Đơn giá: </div>
                            <div class="textbox"><input type="text" name="s_dongia" readonly="readonly" value="30000" /></div>
                        </div>
                        <div class="row">
                            <div class="title"> Tỷ lệ gram/ml: </div>
                            <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="1.5" /></div>
                        </div>
                        <div>
                            <form class="form">
                                <div class="row">
                                    <div class="title"> Mã sản phẩm: </div>
                                    <div class="textbox"><input type="text" name="s_ml_to_gram_factor" readonly="readonly" value="SP_BOT_GIAT_OMO_001" /></div>
                                </div>
                                <div> <button type="submit" name="submit"> Thay đổi thông tin</button>
                            </form>
                        </div>
                    </div>
                </div> -->


            </div>
            <div class="submit_new_product">
                <form action="s32themsanpham.php" method="post">
                    <button type="submit">THÊM SẢN PHẨM MỚI</button>
                </form>
            </div>

        </div>
    </div>
</body>

</html>