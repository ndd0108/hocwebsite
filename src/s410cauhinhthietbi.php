<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HỆ THỐNG MÁY BÁN HÀNG TỰ ĐỘNG - QUẢN LÝ THIẾT BỊ </title>
    <link rel="stylesheet" href="../css/cauhinhthietbi_style.css">

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->


</head>

<body>
    <?php
    // this code for prevent the error: confirm form resubmission... 
    header("Cache-Control: no cache");
    session_cache_limiter("private_no_expire");
    $loginOK = false;
    $user = "";
    session_start();
    if (isset($_SESSION['loginOK'])) {
        $loginOK = $_SESSION['loginOK'];
        $user = $_SESSION['username'];
    }

    if (!$loginOK) header('location: ../index.php');

    require_once 'define.php';
    require_once 'database/db.php';


    //Get the machine_code from parent form
    $codeList  = array();
    $codeList = array_keys($_POST);
    echo '<pre>';
    print_r($codeList);
    echo '</pre>';

    $m_ma_may = "";
    if (!empty($codeList)) {
        $m_ma_may = $codeList[0];
    }

    echo "ma_may: " . $m_ma_may;


    $machineInfo = array();

    function readMachineInfo($m_ma_may)
    {
        $result = array();

        $sql = "SELECT * FROM machine WHERE m_ma_may='$m_ma_may'";

        try {
            // Get DB Object
            $db = new db();
            // Connect
            $db = $db->connect();

            $stmt = $db->query($sql);
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
        } catch (PDOException $e) {
            echo $e->getMessage();
        };
        return $result;
    }




    function createMachineDisplay($machineInfo, $user)
    {
        $m_ma_may = "";
        $productArray = array();

        if (!empty($machineInfo)) {
            $m_ma_may = $machineInfo[0]->m_ma_may;
            $product_machine_list = readProduct_Machine($machineInfo[0]->m_ma_may);
            // echo '<pre>';
            // print_r($product_machine_list);
            // echo '</pre>';
            for ($i = 0; $i < sizeof($product_machine_list); $i++) {
                $productName = readProductName($product_machine_list[$i]->m_ma_sp)[0]->s_name;
                $m_round_to_gram_factor = $product_machine_list[$i]->m_round_to_gram_factor;
                $m_round_to_gram_offset = $product_machine_list[$i]->m_round_to_gram_offset;
                $m_round_to_ml_factor = $product_machine_list[$i]->m_round_to_ml_factor;
                $m_round_to_ml_offset = $product_machine_list[$i]->m_round_to_ml_offset;

                $product = array(
                    "ma_may" => $m_ma_may,
                    "ma_sp" => $product_machine_list[$i]->m_ma_sp,
                    "name" => $productName,
                    "gram_factor" => $m_round_to_gram_factor,
                    "gram_offset" => $m_round_to_gram_offset,
                    "ml_factor"  => $m_round_to_ml_factor,
                    "ml_offset" => $m_round_to_ml_offset
                );

                // echo '<pre>';
                // print_r($product);
                // echo '</pre';

                $productArray[] = $product;
            }
        }

        for ($i = 0; $i < sizeof($productArray); $i++) {


            $str = '
        <div class="content">
        <form class="form" action="s420capnhatthietbi.php" method="post">
        <div class="row">
            <div class = "row_title"> Mã thiết bị: </div>
            <div class = "row_content">
                <input type="text" name="ma_may" value="' . $productArray[$i]["ma_may"] . '" readonly="readonly"/>
            </div>
        </div>
        <div class="row">
            <div class = "row_title"> Tên sản phẩm: </div>
            <div class = "row_content">
                <input type="text" name="s_ten_sp" value="' . $productArray[$i]["name"] . '" readonly="readonly"/>
            </div>
        </div>
        <div class="row">
            <div class = "row_title"> Mã sản phẩm: </div>
            <div class = "row_content">
            <input type="text" name="s_ma_sp" value="' . $productArray[$i]["ma_sp"] . '" readonly="readonly"/>
            </div>
        </div>
        <div class="row">
            <div class = "row_title"> Hệ số round_to_gram_factor: </div>                       
            <div class = "row_content">
            <input type="text" name="gram_factor" value="' . $productArray[$i]["gram_factor"] . '" readonly="readonly"/>
            </div>
        </div>
        <div class="row">
            <div class = "row_title"> Hệ số round_to_gram_offset: </div>                       
            <div class = "row_content">
            <input type="text" name="gram_offset" value="' . $productArray[$i]["gram_offset"] . '" readonly="readonly"/>
            </div>
        </div>
        <div class="row">
            <div class = "row_title"> Hệ số round_to_ml_factor: </div>                       
            <div class = "row_content">
            <input type="text" name="ml_factor" value="' . $productArray[$i]["ml_factor"] . '" readonly="readonly"/>
            </div>
        </div>
        <div class="row">
            <div class = "row_title"> Hệ số ml_to_gram_offset: </div>                       
            <div class = "row_content">
            <input type="text" name="ml_offset" value="' . $productArray[$i]["ml_offset"] . '" readonly="readonly"/>
            </div>
        </div>';
            if ($user === "admin")
                $str .= '
        <div>             
                <button type="submit">THAY ĐỔI THIẾT LẬP </button>            
        </div>';
            $str .= '
        </form>
    </div>';

            echo $str;
        }




        // return $str;
    }





    $machineInfo = readMachineInfo($m_ma_may);
    echo '<pre>';
    print_r($machineInfo);
    echo '</pre>';

    // for ($i = 0; $i < sizeof($machineList); $i++) {
    // createMachineDisplay($machineInfo);
    // }

    ?>
    <div class="background">
        <div class="wrapper">
            <div class="menu">
                <ul>
                    <li><a href="s2main.php">MÀN HÌNH CHÍNH</a></li>
                    <li><a href="s300quanlysanpham.php">QUẢN LÝ SẢN PHẨM</a></li>
                    <li><a class="active" href="s400quanlythietbi.php">QUẢN LÝ THIẾT BỊ</a></li>
                    <li><a href="s500thongke.php">THỐNG KÊ</a></li>
                </ul>
            </div>
            <h1> DANH SÁCH THIẾT BỊ </h1>
            <div class="container">
                <?php
                createMachineDisplay($machineInfo, $user);
                ?>
                <!-- <div class="content">
                    <div class="row">
                        <div class="row_title"> Tên sản phẩm: </div>
                        <div class="row_content">
                            <input type="text" value="BỘT GIẶT OMO" readonly="readonly" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="row_title"> Mã sản phẩm: </div>
                        <div class="row_content">
                            <input type="text" value="SP_BOT_GIAT_OMO_001" readonly="readonly" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="row_title"> Hệ số round_to_gram_factor: </div>
                        <div class="row_content">
                            <input type="text" value="1.2" readonly="readonly" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="row_title"> Hệ số round_to_gram_offset: </div>
                        <div class="row_content">
                            <input type="text" value="-2" readonly="readonly" />
                        </div>
                    </div>
                    <div>
                        <form>
                            <button>THAY ĐỔI THIẾT LẬP </button>
                        </form>
                    </div>
                </div> -->

            </div>
            <div class="submit_new_product">
                <form method="post">
                    <button type="submit">THÊM SẢN PHẨM MỚI</button>
                </form>
            </div>

        </div>
    </div>
</body>

</html>