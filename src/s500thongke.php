<?php
require_once 'define.php';
require_once 'database/db.php';
// this code for prevent the error: confirm form resubmission... 
header("Cache-Control: no cache");
session_cache_limiter("private_no_expire");


require_once 'define.php';
require_once 'database/db.php';


$loginOK = false;

session_start();
if (isset($_SESSION['loginOK']))
    $loginOK = $_SESSION['loginOK'];

if (!$loginOK) header('location: ../index.php');


$machineList = array();

function readMachinesNameList()
{
    $result = array();

    $sql = "SELECT m_ma_may FROM machine";

    try {
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
    return $result;
}





$machineList = readMachinesNameList();
echo '<pre>';
print_r($machineList);
echo '</pre><br>';

$machineListJSON = json_encode($machineList);
echo '<pre>';
print_r($machineListJSON);
echo '</pre><br>';

$dataPoints = array(
    array("x" => 1997, "y" => 254722.1),
    array("x" => 1998, "y" => 292175.1),
    array("x" => 1999, "y" => 369565),
    array("x" => 2000, "y" => 284918.9),
    array("x" => 2001, "y" => 325574.7),
    array("x" => 2002, "y" => 254689.8),
    array("x" => 2003, "y" => 303909),
    array("x" => 2004, "y" => 335092.9),
    array("x" => 2005, "y" => 408128),
    array("x" => 2006, "y" => 300992.2),
    array("x" => 2007, "y" => 401911.5),
    array("x" => 2008, "y" => 299009.2),
    array("x" => 2009, "y" => 319814.4),
    array("x" => 2010, "y" => 357303.9),
    array("x" => 2011, "y" => 353838.9),
    array("x" => 2012, "y" => 288386.5),
    array("x" => 2013, "y" => 485058.4),
    array("x" => 2014, "y" => 326794.4),
    array("x" => 2015, "y" => 483812.3),
    array("x" => 2016, "y" => 254484)
);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>HỆ THỐNG MÁY BÁN HÀNG TỰ ĐỘNG - THỐNG KÊ</title>
    <link rel="stylesheet" href="../css/thongke_style.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script charset="UTF-8">
        $(function() {

            // DRAW CHART



            //==========================



            $.fn.myFunction = function(selectedMachine) {
                var result = "";
                var myLink = "../api/getavailableproduct/" + selectedMachine;
                $.get(myLink, function(data, status) {
                    // alert(data);
                    var products = jQuery.parseJSON(data);
                    $('.product_selector')[0].options.length = 0;
                    $(products).each(function(i, val) {
                        $.each(val, function(k, v) {
                            // alert(k + ":" + v);
                            var o = new Option(v, v);
                            $(".product_selector").append(o);

                        })
                    })
                });
            }




            // $.get("../api/exam/Dũng/Nguyễn", function(data, status) {
            //     alert("Data: " + data + "\nStatus: " + status);
            // });


            // Load all available machine and display in the Combobox
            var machine1 = jQuery.parseJSON('<?php echo $machineListJSON ?>');
            $('.machine_selector')[0].options.length = 0;
            $(machine1).each(function(i, val) {
                $.each(val, function(k, v) {
                    // alert(i + ": " + val + " : " + k + ":" + v);
                    var o = new Option(v, v);
                    $(".machine_selector").append(o);
                    if (i == 0) {
                        $(".machine_selector").val(v).change();
                        $.fn.myFunction(v);

                    }
                })
            })

            // $(".datepicker").datepicker({
            //                 changeMonth: true,
            //                 changeYear: true,
            //                 showButtonPanel: true,
            //                 dateFormat: 'yy-mm',
            //                 onClose: function(dateText, inst) {
            //                     var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            //                     var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            //                     $(this).datepicker('setDate', new Date(year, month, 1));
            //                 }
            //             });



            // Load the Avaiable product of machine when machine combobox selected
            $(".machine_selector").change(function() {
                var selectedMachine = $(this).children("option:selected").val();
                $.fn.myFunction(selectedMachine);

            });


            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,

                onClose: function(dateText, inst) {
                    var v = $(this).val();
                    console.log("value default: " + v);
                    $(this).datepicker("option", "defaultDate", new Date(v));
                    $(this).datepicker("setDate", new Date(v));
                }
            });
            $(".datepicker").focus(function() {
                $(".ui-datepicker-calendar").show();
                $("#ui-datepicker-div").position({
                    my: "center top",
                    at: "center bottom",
                    of: $(this)
                });
            });
            $(".frame2").show();



            // $(".datepicker").datepicker({
            //     dateFormat: 'yy-mm-dd',
            //     changeMonth: true,
            //     changeYear: true,
            //     showButtonPanel: true,

            //     onClose: function(dateText, inst) {
            //         var v = $(this).val();
            //         console.log("value: " + v);
            //         $(this).datepicker("option", "defaultDate", new Date(v));
            //         $(this).datepicker("setDate", new Date(v));
            //     }
            // });
            // $(".datepicker").focus(function() {
            //     $(".ui-datepicker-calendar").show();
            //     $("#ui-datepicker-div").position({
            //         my: "center top",
            //         at: "center bottom",
            //         of: $(this)
            //     });
            // });
            // $(".frame2").show();;

            // Action when the year-month-day-time type changed
            // $(".frame2").hide();
            $(".type_selector").change(function() {
                var selectedType = $(this).children("option:selected").val();
                console.log(selectedType);


                switch (selectedType) {
                    case "year":
                        console.log("You have selected type: " + "Năm");
                        // $(".datepicker").datepicker("option", "dateFormat", "yy");


                        $(".datepicker").datepicker("option", {
                            dateFormat: 'yy',
                            // changeMonth: true,
                            // changeYear: true,
                            // showButtonPanel: true,

                            onClose: function(dateText, inst) {
                                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                // $(this).val($.datepicker.formatDate('yy', new Date(year, month, 1)));                                                              
                                $(this).datepicker("option", "defaultDate", new Date(year, 0, 1));
                                $(this).datepicker("setDate", new Date(year, 0, 1));
                            }
                        });


                        $(".datepicker").focus(function() {
                            $(".ui-datepicker-calendar").hide();
                            $("#ui-datepicker-div").position({
                                my: "center top",
                                at: "center bottom",
                                of: $(this)
                            });
                        });
                        $(".frame2").show();
                        break;

                    case "month":
                        console.log("You have selected type: " + "Thang");
                        // $(".datepicker").datepicker("option", "dateFormat", "yy-mm");
                        $(".datepicker").datepicker("option", {
                            dateFormat: 'yy-mm',
                            // changeMonth: true,
                            // changeYear: true,
                            // showButtonPanel: true,

                            onClose: function(dateText, inst) {
                                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                var v = $(this).val();
                                console.log("value month: " + v);
                                console.log("year month: " + year + " " + month);
                                $(this).datepicker("option", "defaultDate", new Date(year, month, 1));
                                $(this).datepicker("setDate", new Date(year, month, 1));
                            }
                        });
                        $(".datepicker").focus(function() {
                            $(".ui-datepicker-calendar").hide();
                            $("#ui-datepicker-div").position({
                                my: "center top",
                                at: "center bottom",
                                of: $(this)
                            });
                        });
                        $(".frame2").show();
                        break;

                    case "day":
                        // alert("You have selected type: " + "ngày");
                        // $(".datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
                        console.log(" you selected: ngay");
                        $(".datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
                        $(".datepicker").datepicker("option", {
                            dateFormat: 'yy-mm-dd',
                            // changeMonth: true,
                            // changeYear: true,
                            // showButtonPanel: true,

                            onClose: function(dateText, inst) {
                                var v = $(this).val();
                                console.log("value day: " + v);
                                $(this).datepicker("option", "defaultDate", new Date(v));
                                $(this).datepicker("setDate", new Date(v));
                            }
                        });
                        $(".datepicker").focus(function() {
                            $(".ui-datepicker-calendar").show();
                            $("#ui-datepicker-div").position({
                                my: "center top",
                                at: "center bottom",
                                of: $(this)
                            });
                        });
                        $(".frame2").show();
                        break;
                    case "hour":
                        // alert("You have selected type: " + "giờ");
                        // $(".datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
                        console.log(" you selected: gio");
                        $(".datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
                        $(".datepicker").datepicker("option", {
                            dateFormat: 'yy-mm-dd',
                            // changeMonth: true,
                            // changeYear: true,
                            // showButtonPanel: true,

                            onClose: function(dateText, inst) {
                                var v = $(this).val();
                                console.log("value: " + v);
                                $(this).datepicker("option", "defaultDate", new Date(v));
                                $(this).datepicker("setDate", new Date(v));
                            }
                        });
                        $(".datepicker").focus(function() {
                            $(".ui-datepicker-calendar").show();
                            $("#ui-datepicker-div").position({
                                my: "center top",
                                at: "center bottom",
                                of: $(this)
                            });
                        });

                        $(".frame2").hide();
                        break;

                    default:
                        break;
                }
            });


            // ==============================================
            // FUNCTION OF DRAWING CHARTS
            //===============================================

            $.fn.updateTable = function(gramData, moneyData) {
                // console.log("arrayData: s");
                $("#datatable").find("tr:gt(0)").remove(); // clear table
                for (var i = 0; i < gramData.length; i++) {
                    // console.log(arrayData[i]);
                    // console.log(arrayData[i].label+ " " + arrayData[i].y);
                    $('#datatable').append('<tr><td>' + gramData[i].label + '</td> <td>' + gramData[i].y + '</td> <td>' + moneyData[i].y + '</td> </tr>')
                }
            }

            $.fn.drawChart = function(chartID, data, chartType, chartTitle, xTitle, yTitle) {
                var dataV = [];
                var dataSeries = {
                    type: chartType
                };
                var dataPoints = [];
                dataSeries.dataPoints = data;
                dataV.push(dataSeries);
                var option = {
                    animationEnabled: true,
                    theme: "light2",
                    title: {
                        text: chartTitle
                    },
                    axisX: {
                        title: xTitle,
                        crosshair: {
                            enabled: true,
                            snapToDataPoint: true
                        }
                    },
                    axisY: {
                        title: yTitle,
                        crosshair: {
                            enabled: true,
                            snapToDataPoint: true
                        }
                    },
                    data: dataV
                }

                var chart = new CanvasJS.Chart(chartID, option);
                chart.render();
            }

            $.fn.typeHourDrawing = function() {
                console.log("HELLO");
                var ma_may = $(".machine_selector").children("option:selected").val();
                var ma_sp = $(".product_selector").children("option:selected").val();

                var startDay = $('#startday').val();
                var myLink = "../api/getDiaryData/" + "hour" + "/" + ma_may + "/" + ma_sp + "/" + startDay;


                var gramData = [];
                var moneyData = [];
                $.get(myLink, function(data, status) {
                    console.log("get data: " + data);
                    contentData = jQuery.parseJSON(data);
                    console.log(gramData);
                    $(contentData).each(function(i, val) {
                        var gr = {};
                        var mn = {};
                        gr.label = val.label;
                        mn.label = val.label;
                        gr.y = parseInt(val.gram);
                        mn.y = parseInt(val.money);
                        gramData.push(gr);
                        moneyData.push(mn);
                    });
                    console.log(gramData);
                    $.fn.updateTable(gramData, moneyData);

                    $.fn.drawChart("chartContainer1", gramData, "column",
                        "KHỐI LƯỢNG SẢN PHẨM MUA TRONG NGÀY",
                        "THỜI GIAN",
                        "KHỐI LƯỢNG(GRAM)"
                    );
                    $.fn.drawChart("chartContainer2", moneyData, "column",
                        "SỐ TIỀN MUA SẢN PHẨM TRONG NGÀY",
                        "THỜI GIAN",
                        "LƯỢNG TIỀN(VND)"
                    );

                });

            }

            $.fn.typeDayDrawing = function() {
                console.log("HELLO");
                var ma_may = $(".machine_selector").children("option:selected").val();
                var ma_sp = $(".product_selector").children("option:selected").val();

                var startDay = $('#startday').val();
                var endDay = $('#endday').val();
                var myLink = "../api/getDiaryData/" + "day" + "/" + ma_may + "/" + ma_sp + "/" + startDay + "/" + endDay;

                console.log(myLink);

                var gramData = [];
                var moneyData = [];
                $.get(myLink, function(data, status) {
                    console.log("get data: " + data);
                    contentData = jQuery.parseJSON(data);
                    console.log(gramData);
                    $(contentData).each(function(i, val) {
                        var gr = {};
                        var mn = {};
                        gr.label = val.label;
                        mn.label = val.label;
                        gr.y = parseInt(val.gram);
                        mn.y = parseInt(val.money);
                        gramData.push(gr);
                        moneyData.push(mn);
                    });
                    console.log(gramData);
                    $.fn.updateTable(gramData, moneyData);

                    $.fn.drawChart("chartContainer1", gramData, "column",
                        "KHỐI LƯỢNG SẢN PHẨM MUA THEO NGÀY",
                        "THỜI GIAN",
                        "KHỐI LƯỢNG (GRAM)"
                    );
                    $.fn.drawChart("chartContainer2", moneyData, "column",
                        "SỐ TIỀN MUA SẢN PHẨM THEO NGÀY",
                        "THỜI GIAN",
                        "LƯỢNG TIỀN (VND)"
                    );
                });
            }

            $.fn.typeMonthDrawing = function() {
                console.log("HELLO");
                var ma_may = $(".machine_selector").children("option:selected").val();
                var ma_sp = $(".product_selector").children("option:selected").val();

                var startDay = $('#startday').val();
                var endDay = $('#endday').val();
                var myLink = "../api/getDiaryData/" + "month" + "/" + ma_may + "/" + ma_sp + "/" + startDay + "/" + endDay;

                console.log(myLink);

                var gramData = [];
                var moneyData = [];
                $.get(myLink, function(data, status) {
                    console.log("get data: " + data);
                    contentData = jQuery.parseJSON(data);
                    console.log(gramData);
                    $(contentData).each(function(i, val) {
                        var gr = {};
                        var mn = {};
                        gr.label = val.label;
                        mn.label = val.label;
                        gr.y = parseInt(val.gram);
                        mn.y = parseInt(val.money);
                        gramData.push(gr);
                        moneyData.push(mn);
                    });
                    console.log(gramData);
                    $.fn.updateTable(gramData, moneyData);

                    $.fn.drawChart("chartContainer1", gramData, "column",
                        "KHỐI LƯỢNG SẢN PHẨM MUA THEO THÁNG",
                        "THỜI GIAN",
                        "KHỐI LƯỢNG (GRAM)"
                    );
                    $.fn.drawChart("chartContainer2", moneyData, "column",
                        "SỐ TIỀN MUA SẢN PHẨM THEO THÁNG",
                        "THỜI GIAN",
                        "LƯỢNG TIỀN (VND)");
                });
            }

            $.fn.typeYearDrawing = function() {
                console.log("HELLO");
                var ma_may = $(".machine_selector").children("option:selected").val();
                var ma_sp = $(".product_selector").children("option:selected").val();

                var startDay = $('#startday').val();
                var endDay = $('#endday').val();
                var myLink = "../api/getDiaryData/" + "year" + "/" + ma_may + "/" + ma_sp + "/" + startDay + "/" + endDay;

                console.log(myLink);

                var gramData = [];
                var moneyData = [];
                $.get(myLink, function(data, status) {
                    console.log("get data: " + data);
                    contentData = jQuery.parseJSON(data);
                    console.log(gramData);
                    $(contentData).each(function(i, val) {
                        var gr = {};
                        var mn = {};
                        gr.label = val.label;
                        mn.label = val.label;
                        gr.y = parseInt(val.gram);
                        mn.y = parseInt(val.money);
                        gramData.push(gr);
                        moneyData.push(mn);
                    });
                    console.log(gramData);
                    $.fn.updateTable(gramData, moneyData);

                    $.fn.drawChart("chartContainer1", gramData, "column",
                        "KHỐI LƯỢNG SẢN PHẨM MUA THEO NĂM",
                        "THỜI GIAN",
                        "KHỐI LƯỢNG(GRAM)");
                    $.fn.drawChart("chartContainer2", moneyData, "column",
                        "SỐ TIỀN MUA SẢN PHẨM THEO NĂM",
                        "THỜI GIAN",
                        "LƯỢNG TIỀN(VND)");
                });
            }

            $(".button").click(function() {
                var type = $(".type_selector").children("option:selected").val();
                switch (type) {
                    case 'hour':
                        $.fn.typeHourDrawing();
                        break;
                    case 'day':
                        $.fn.typeDayDrawing();
                        break;
                    case 'month':
                        $.fn.typeMonthDrawing();
                        break;
                    case 'year':
                        $.fn.typeYearDrawing();
                        break;
                    default:
                        break;
                }
            })
        });
    </script>
</head>

<body>
    <?php
    ?>

    <div class="background">
        <div class="wrapper">
            <div class="menu">
                <ul>
                    <li><a href="s2main.php">MÀN HÌNH CHÍNH</a></li>
                    <li><a href="s300quanlysanpham.php">QUẢN LÝ SẢN PHẨM</a></li>
                    <li><a href="s400quanlythietbi.php">QUẢN LÝ THIẾT BỊ</a></li>
                    <li><a class="active" href="s500thongke.php">THỐNG KÊ</a></li>
                </ul>
            </div>
            <div class="container">
                <h3> THỐNG KÊ </h3>
                <div class="row">
                    <div class="frame">
                        <div class="title">
                            Mã thiết bị
                        </div>
                        <select class="machine_selector">
                            <!-- <option value="MACHINE_1">MACHINE1</option> -->
                        </select>
                    </div>
                    <div class="frame">
                        <div class="title_start">
                            Mã sản phẩm
                        </div>
                        <select class="product_selector">
                            <option value="OMO">OMO</option>
                        </select>
                    </div>
                    <div class="frame">
                        <div class="title_end">
                            Thống kê theo
                        </div>
                        <select class="type_selector">
                            <option value="hour">Giờ</option>
                            <option value="day" selected="true">Ngày</option>
                            <option value="month">Tháng </option>
                            <option value="year"> Năm </option>
                        </select>
                    </div>
                    <div class="frame1">
                        <div class="title">
                            Start
                        </div>
                        <input type="text" id="startday" class="datepicker" />
                    </div>
                    <div class="frame2">
                        <div class="title">
                            End
                        </div>
                        <input type="text" id="endday" class="datepicker" />
                    </div>
                    <!-- <div class="clear"></div> -->
                </div>

                <div>
                    <button class="button">XÁC NHẬN</button>
                </div>
            </div>
            <div id="divtable">
                <table id="datatable">
                    <caption> DỮ LIỆU </caption>
                    <thead>
                        <tr>
                            <th> Thời gian </th>
                            <th> Lượng mua (gram) </th>
                            <th> Lượng tiền (VNĐ) </th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div id="chartContainer1" class="chartContainer">

            </div>

            <div id="chartContainer2" class="chartContainer">

            </div>

        </div>
    </div>
</body>

</html>