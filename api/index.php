<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require_once '../src/database/db.php';
$app = new \Slim\App;

$app->get('/hello', function () {
    echo 'hello';
});

$app->get('/images', function () {
    echo 'hello';
});

// $app->get('/hello/{name}', function (Request $request, Response $response) {
//     $first = $request->getAttribute('name');;
//     echo 'hello' . $first;
// });


$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello___$name");

    return $response;
});

$app->get('/images/{file}', function () {
    header('location: ../index.php');
});

$app->get('/getavailableproduct/{machine_id}', function (Request $request, Response $response) {
    $m_ma_may = $request->getAttribute('machine_id');

    // echo $m_ma_may;

    $sql = "SELECT m_ma_sp FROM machine_sanpham_relation WHERE m_ma_may='$m_ma_may'";

    try {
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($result);
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
});

$app->get('/getallparams/{machine_id}', function (Request $request, Response $response) {
    $m_ma_may = $request->getAttribute('machine_id');
    // echo $m_ma_may . '<br>';
    $kq1 = readParamOfMachine($m_ma_may);
    echo (json_encode($kq1));
    // echo '<pre>';
    // print_r (json_encode($kq1));
    // echo '</pre>';
});


$app->get('/getDiaryData/hour/{machine_id}/{product_id}/{startday}', function (Request $request, Response $response) {
    $ma_may = $request->getAttribute('machine_id');
    $ma_sp = $request->getAttribute('product_id');
    $startday = $request->getAttribute('startday');


    // echo $m_ma_may;

    $sql = "SELECT d_time, d_gram, d_so_tien FROM machine_sanpham_diary 
            WHERE ma_may='$ma_may' AND ma_sp='$ma_sp' AND date(d_time)='$startday' ORDER BY d_time";

    // echo $sql . '<br>';

    try {

        $result = array();
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();



        $stmt = $db->query($sql);
        $tags = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // echo '<pre>';
        //     print_r($tags);
        // echo '</pre>';

        $tags = array_map(function ($tag) {
            return array(
                'label' => date('H:i:s', strtotime($tag['d_time'])),
                'gram' => $tag['d_gram'],
                'money' => $tag['d_so_tien']
            );
        }, $tags);

        $result = json_encode($tags);


        echo $result;
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
});

$app->get('/getDiaryData/day/{machine_id}/{product_id}/{startday}/{endday}', function (Request $request, Response $response) {
    $ma_may = $request->getAttribute('machine_id');
    $ma_sp = $request->getAttribute('product_id');
    $startday = $request->getAttribute('startday');
    $endday = $request->getAttribute('endday');

    // echo "startday: " . $startday . '<br>';
    // echo "endday: " . $endday . '<br>';

    $begin = new DateTime($startday);
    $end   = new DateTime($endday);

    $output = array();
    try {
        $result = array();
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
            $date = $i->format('Y-m-d');
            // echo $date . "<br>";
            $sql = "SELECT date(d_time), SUM(d_gram), SUM(d_so_tien) FROM machine_sanpham_diary WHERE 
                            ma_may='$ma_may' AND ma_sp='$ma_sp' AND date(d_time)='$date'";
            $stmt = $db->query($sql);
            $tags = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $tags[0]['date(d_time)'] = $date;

            $tags = array_map(function ($tag) {
                return array(
                    'label' => $tag['date(d_time)'],
                    'gram' => ($tag['SUM(d_gram)'] == NULL ? "0" : $tag['SUM(d_gram)']),
                    'money' => ($tag['SUM(d_so_tien)'] == NULL ? "0" : $tag['SUM(d_so_tien)'])
                );
            }, $tags);

            // echo '<pre>';
            // print_r($tags);
            // echo '</pre>';

            $result[] = $tags[0];
        }
        // echo '<pre>';
        echo (json_encode($result));
        // echo '</pre>';
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
});



$app->get('/getDiaryData/month/{machine_id}/{product_id}/{startday}/{endday}', function (Request $request, Response $response) {
    $ma_may = $request->getAttribute('machine_id');
    $ma_sp = $request->getAttribute('product_id');
    $startday = $request->getAttribute('startday');
    $endday = $request->getAttribute('endday');


    $begin = new DateTime($startday);
    $end   = new DateTime($endday);




    $output = array();
    try {
        $result = array();
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        for ($i = $begin; $i <= $end; $i->modify('+1 month')) {
            $date = $i->format('Y-m');
            // echo $date . "<br>";
            $sql = "SELECT date_format(d_time,'%Y-%m'), SUM(d_gram), SUM(d_so_tien) FROM machine_sanpham_diary WHERE 
                            ma_may='$ma_may' AND ma_sp='$ma_sp' AND date_format(d_time,'%Y-%m')='$date'";
            $stmt = $db->query($sql);
            $tags = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $tags[0]['date_format(d_time,\'%Y-%m\')'] = $date;

            $tags = array_map(function ($tag) {
                return array(
                    'label' => $tag['date_format(d_time,\'%Y-%m\')'],
                    'gram' => ($tag['SUM(d_gram)'] == NULL ? "0" : $tag['SUM(d_gram)']),
                    'money' => ($tag['SUM(d_so_tien)'] == NULL ? "0" : $tag['SUM(d_so_tien)'])
                );
            }, $tags);

            // echo '<pre>';
            // print_r($tags);
            // echo '</pre>';

            $result[] = $tags[0];
        }
        // echo '<pre>';
        echo (json_encode($result));
        // echo '</pre>';
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
});


$app->get('/getDiaryData/year/{machine_id}/{product_id}/{startday}/{endday}', function (Request $request, Response $response) {
    $ma_may = $request->getAttribute('machine_id');
    $ma_sp = $request->getAttribute('product_id');
    $startday = $request->getAttribute('startday');
    $endday = $request->getAttribute('endday');

    $begin = (int)$startday;
    $end   = (int)$endday;

    $output = array();
    try {
        $result = array();
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        for ($i = $begin; $i <= $end; $i++) {
            $date = $i;
            $sql = "SELECT date_format(d_time,'%Y'), SUM(d_gram), SUM(d_so_tien) FROM machine_sanpham_diary WHERE 
                            ma_may='$ma_may' AND ma_sp='$ma_sp' AND date_format(d_time,'%Y')='$date'";
            $stmt = $db->query($sql);
            $tags = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $tags[0]['date_format(d_time,\'%Y\')'] = $date;

            $tags = array_map(function ($tag) {
                return array(
                    'label' => $tag['date_format(d_time,\'%Y\')'],
                    'gram' => ($tag['SUM(d_gram)'] == NULL ? "0" : $tag['SUM(d_gram)']),
                    'money' => ($tag['SUM(d_so_tien)'] == NULL ? "0" : $tag['SUM(d_so_tien)'])
                );
            }, $tags);

            // echo '<pre>';
            // print_r($tags);
            // echo '</pre>';

            $result[] = $tags[0];
        }
        // echo '<pre>';
        echo (json_encode($result));
        // echo '</pre>';
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
});



$app->get('/sanpham/{ma_sp}', function (Request $request, Response $response) {
    $s_ma_sp = $request->getAttribute('ma_sp');

    // echo $m_ma_may;

    $sql = "SELECT * FROM sanpham WHERE s_ma_sp='$s_ma_sp'";

    try {
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($result);
    } catch (PDOException $e) {
        echo $e->getMessage();
    };
});



$app->post("/updateDiary", function (Request $request, Response $response) {
    $ma_may = $request->getParam('ma_may');
    $ma_sp = $request->getParam('ma_sp');
    $d_time = $request->getParam('d_time');
    $d_gram = $request->getParam('d_gram');
    $d_ml = $request->getParam('d_ml');
    $d_so_tien = $request->getParam('d_so_tien');

    // echo $ma_may . '<br>';
    // echo $ma_sp . '<br>';
    // echo $d_time . '<br>';
    // echo $d_gram . '<br>';
    // echo $d_ml . '<br>';
    // echo $d_so_tien . '<br>';

    $sql = "INSERT INTO machine_sanpham_diary (ma_may, ma_sp, d_time, d_gram, d_ml, d_so_tien)
                    VALUE ('$ma_may', '$ma_sp', '$d_time', '$d_gram', '$d_ml', '$d_so_tien') ";

    // echo $sql . '<br>';

    try {
        $db = new db();

        $db = $db->connect();
        $stmt = $db->prepare($sql);

        $stmt->execute();

        echo 'added';
        $db = null;
    } catch (PDOException $e) {
        echo "err: " . $e->getMessage();
    }
});


$app->post("/readAccount", function(Request $request, Response $response){
    $user = $request->getParam("user");
    $pass = $request->getParam("pass");
    $pass = md5($pass);
    if (readAccount($user, $pass)){
        echo "success";
    }
});

$app->post("/postMotorParameter", function(Request $request, Response $response){
    $ma_may = $request->getParam("ma_may");
    $ma_sp = $request->getParam("ma_sp");
    $vong1 = $request->getParam("vong1");
    $khoiluong1 = $request->getParam("khoiluong1");
    $vong2 = $request->getParam("vong2");
    $khoiluong2 = $request->getParam("khoiluong2");
    $vong3 = $request->getParam("vong3");
    $khoiluong3 = $request->getParam("khoiluong3");

    echo $ma_may . "<br>";
    echo $ma_sp . "<br>";
    echo $vong1 . "<br>";
    echo $khoiluong1 . "<br>";
    echo $vong2 . "<br>";
    echo $khoiluong2 . "<br>";
    echo $vong3 . "<br>";
    echo $khoiluong3 . "<br>";
});

$app->run();
